from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from .models import Register
def update_invoice_detail_view(request):
    data = {
        "user": "rosana",
        "number": 125,
        "client": "Pedro Pérez",
        "description": "Venta de 3 teclados",
        "tax_rates": "16,00 %",
        "total_invoice": "69.600.000,00",
        "base": "60.000.000,00"
    }
    return JsonResponse(data)
